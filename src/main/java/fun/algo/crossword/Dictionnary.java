package fun.algo.crossword;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.immutables.value.Value;
import org.immutables.value.Value.Parameter;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSetMultimap;
import com.google.common.collect.Sets;

import io.atlassian.fugue.Pair;
import io.atlassian.fugue.extras.ImmutableMaps;

public final class Dictionnary {
	@Value.Immutable
	public static interface Criterion {
		@Parameter
		int idx();

		@Parameter
		char letter();
	}

	@Value.Immutable
	public static interface Size {
		@Parameter
		int size();
	}

	private final ImmutableMap<ImmutableSize, ImmutableSetMultimap<ImmutableCriterion, String>> dictionary;
	private final ImmutableMultimap<ImmutableSize, String> bySize;

	private Dictionnary(ImmutableMap<ImmutableSize, ImmutableSetMultimap<ImmutableCriterion, String>> dictionary, ImmutableMultimap<ImmutableSize, String> bySize) {
		this.dictionary = dictionary;
		this.bySize=bySize;
	}

	public ImmutableCollection<String> wordsOfLength(ImmutableSize size) {
		return bySize.get(size);
	}

	public static final ImmutableCriterion criterion(int idx, char letter) {
		return ImmutableCriterion.of(idx,letter);
	}

	public static final ImmutableSize size(int size) {
		return ImmutableSize.of(size);
	}

	public ImmutableSet<String> wordsFor(ImmutableSize size, ImmutableCriterion first, ImmutableCriterion... criterions) {
		ImmutableSetMultimap<ImmutableCriterion, String> subDictionary = dictionary.get(size);
		Set<String> result = subDictionary.get(first);
		for (ImmutableCriterion criterion : criterions) {
			result = Sets.intersection(subDictionary.get(criterion), result);
		}
		return ImmutableSet.copyOf(result);
	}

	public static Dictionnary from(Path p) throws IOException {
		return fromStream(Files.lines(p));
	}

	@VisibleForTesting
	static Dictionnary from(String... words) {
		return fromStream(Stream.of(words));
	}

	private static Dictionnary fromStream(Stream<String> lines) {
		ImmutableMultimap<ImmutableSize, String> wordsBySize = lines
				.<ImmutableMultimap.Builder<ImmutableSize, String>>collect(
				ImmutableMultimap::builder,
				(builder,word)->builder.put(ImmutableSize.of(word.length()),word),
				(a,b)->a.putAll(b.build()))
			.build();

		HashMap<ImmutableSize, HashMultimap<ImmutableCriterion, String>> hashMap=wordsBySize.entries().parallelStream()
				.flatMap(e->
					IntStream.range(0, e.getValue().length())
						.mapToObj(idx -> new Pair<>(
								e.getKey(),//first index: word size
								new Pair<>(// then second index: criterion
										ImmutableCriterion.of(idx, e.getValue().charAt(idx)),
										e.getValue())
					)))
				.collect(
					HashMap::new,
					//add while creating the subMap if needed.
					(map, criterionAndWord) -> map
							.computeIfAbsent(criterionAndWord.left(), l -> HashMultimap.create())
							.put(criterionAndWord.right().left(), criterionAndWord.right().right()),
					//add all entries from B in A
					(a, b) -> b.forEach((size, wordMap) -> a.merge(size, wordMap, (wordMap1, wordMap2) -> {
						wordMap1.putAll(wordMap2);
						return wordMap1;
					})));

		//Immutabilize all the things
		return new Dictionnary(ImmutableMaps.transformValue(hashMap, ImmutableSetMultimap::copyOf),wordsBySize);
	}

}
