package fun.algo.crossword;

import static com.google.common.truth.Truth.assert_;
import static fun.algo.crossword.Dictionnary.criterion;
import static fun.algo.crossword.Dictionnary.size;

import org.junit.Test;

public class DictionnaryTest {

	@Test
	public void testWordsFor() throws Exception {
		Dictionnary dictionnary = Dictionnary.from("abc", "bcd", "abe", "abcd");
		assert_().that(dictionnary.wordsOfLength(size(3))).containsExactly("abc", "bcd", "abe");
		assert_().that(dictionnary.wordsOfLength(size(4))).containsExactly("abcd");
		assert_().that(dictionnary.wordsFor(size(3), criterion(0, 'a'))).containsExactly("abc", "abe");
		assert_().that(dictionnary.wordsFor(size(4), criterion(0, 'a'))).containsExactly("abcd");
		assert_().that(dictionnary.wordsFor(size(3), criterion(0, 'a'), criterion(2, 'e'))).containsExactly("abe");
	}

}
